angular.module('videoService', ['ngRoute', 'infinite-scroll'])

.controller('SearchController', function($scope, Themoviedb) {
    $scope.search = '';
    $scope.submit = function() {
        $scope.$emit('toParent', $scope.search);
        /*$scope.themoviedb.search = $scope.search;*/
        $scope.themoviedb.nextPage();
    };
})

.controller('MoviesShowController', function($scope, $rootScope, Themoviedb) {
    $scope.$on('toParent', function (event, data) {
        $scope.search = data;
        console.log(data); // 'Some data'
    });

    $scope.$broadcast('toChild', $scope.search);
    $scope.$on('toChild', function (event, data) {
        this.search = data;
        console.log('some data: ' + data); // 'Some data'
    });


    $scope.themoviedb = new Themoviedb($scope);

  /*console.log($scope.themoviedb);*/
})

.controller('MovieDetailsController', function($scope, $http, $routeParams){
    var apiKey = 'f7fe3dfebcc7fac74fb816b869236130';
    var controller = this;
    var movieId;

    $http({method: 'GET', url: '/film/' + $routeParams.id})
    .success(function(data){
        controller.movieId = $routeParams.id;
        console.log(controller.movieId);

        $http.get("https://api.themoviedb.org/3/movie/"+ controller.movieId + "?api_key=" + apiKey)
        .then(function(response) {
            controller.details = response.data;
        });
    });
})

.factory('Themoviedb', function($http, $rootScope) {
  var Themoviedb = function() {

    this.search;
    this.items = [];
    this.stopLoading = false;
    this.currentPage = 0;
  };

  Themoviedb.prototype.nextPage = function() {


    if (this.stopLoading) {
        return;
    }

    this.stopLoading = true;
    this.currentPage++;
    var apiKey = 'f7fe3dfebcc7fac74fb816b869236130';

    if (this.search == undefined) {
        var url = "https://api.themoviedb.org/3/movie/popular?page=" + this.currentPage + "&api_key=" + apiKey;
    } else {
        var url = "https://api.themoviedb.org/3/search/multi?query=" + this.search + "&page=" + this.currentPage + "&api_key=" + apiKey;
    }

    $http.get(url).then(function(response) {
      var items = response.data;

      for (var i = 0; i < items.results.length; i++) {
        this.items.push(items.results[i]);
      }

      this.currentPage < response.data.total_pages ? this.stopLoading = false : this.stopLoading = true;

      console.log("Current page: " + this.currentPage);
      console.log("Current response url: " + url);
      console.log(items);
      console.log(this.search);

    }.bind(this));

  };

  return Themoviedb;
});




/*angular.module('videoService', ['ngRoute', 'infinite-scroll'])
  .controller('MoviesShowController', function($scope, $http) {
    $scope.$watch('search', function() {
      fetch();
    });

    $scope.search = "Sherlock Holmes";

    $scope.apiKey = 'f7fe3dfebcc7fac74fb816b869236130';
    $scope.$on('emitFromChild', function (event, data) {
 $scope.search = data;
 console.log($scope.search);
 });


    function fetch() {



      $http.get("https://api.themoviedb.org/3/search/multi?query=" + $scope.search + "&page=" + $scope.currentPage + "&api_key=" + $scope.apiKey)
        .then(function(response) {
          $scope.items = response.data;
          console.log($scope.search);
          console.log($scope.items);
        });

      $http.get("https://api.themoviedb.org/3/search/multi?query=" + $scope.search + "&api_key=" + $scope.apiKey)
        .then(function(response) {
          $scope.items = response.data;
        });
    }

    $scope.update = function(movie) {
      $scope.search = movie.title;
    };

    $scope.select = function() {
        $scope.$emit('emitFromChild', $scope.search);
        console.log($scope.search);
        console.log("https://api.themoviedb.org/3/search/multi?query=" + $scope.search + "&api_key=" + $scope.apiKey);
        this.setSelectionRange(0, this.value.length);
        console.log(this.setSelectionRange);
    }


  });*/