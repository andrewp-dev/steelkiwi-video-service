angular.module('videoService')
.config(function($routeProvider) {
    $routeProvider/*.when('/film/:id', {
        templateUrl: '/templates/pages/details/index.html',
        controller: 'MovieDetailsController',
        controllerAs: 'showController'
    })*/
    .when('/', {
        templateUrl: '/templates/pages/list/index.html',
        controller: 'MoviesShowController'
    })
    .otherwise({redirect: '/'});
});