var gulp = require('gulp');
var less = require('gulp-less');
var watch = require('gulp-watch');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var webserver = require('gulp-webserver');
var path = require('path');

gulp.task('default', function() {
  gulp.src('app')
    .pipe(webserver({
        fallback: 'index.html',
        livereload: true,
        directoryListing: false,
        open: true
    }));

    return gulp.src('src/less/**/*.less')
        .pipe(watch('src/less/*.less'))
        .pipe(less({
          paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest('app/css'));
});

gulp.task('css-min', function () {
    gulp.src('app/css/style.css')
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('app/css/'));
});
